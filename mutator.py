import time
import rtmidi
import random
import sys
import platform
import copy

if int(platform.python_version_tuple()[0])>2:
	from tkinter import *
	from tkinter.filedialog import *
	from tkinter.messagebox import *
else:
	from Tkinter import *
	from tkFileDialog import *
	from tkMessageBox import *

version = "0.0.1"

def sq(x): return x*x

class patch:
    def __init__(self):
        self.num_ops=5
        self.num_envpts=6
        self.num_outs=2
        
        self.ops=[]
        for i in range(0,self.num_ops):
            op={}
            op["pitch"]=16
            op["volume"]=127
            op["shape"]=0
            op["fixed"]=0
            op["env"]=[]
            for j in range(0,self.num_envpts):
                op["env"].append([0,1])
            op["out"]=[]
            for j in range(0,self.num_outs):
                op["out"].append(21)
            self.ops.append(op)

    def randomise(self):
        for op in self.ops:
            op["pitch"]=random.random()*127
            op["volume"]=random.random()*127
            op["fixed"]=int(random.random()*2)
            op["shape"]=int(random.random()*7)
            for i in range(0,len(op["env"])):
                op["env"][i][0]=random.random()
                op["env"][i][1]=random.random()
            for i in range(0,len(op["out"])):
                op["out"][i]=int(random.random()*10)

    def mutate(self,rate):
        for op in self.ops:
            if random.random()<rate: op["pitch"]=random.random()*127
            if random.random()<rate: op["volume"]=random.random()*127
            if random.random()<rate: op["fixed"]=int(random.random()*2)
            if random.random()<rate: op["shape"]=int(random.random()*7)
            for i in range(0,len(op["env"])):
                if random.random()<rate: op["env"][i][0]=random.random()
                if random.random()<rate: op["env"][i][1]=random.random()
            for i in range(0,len(op["out"])):
                if random.random()<rate: op["out"][i]=int(random.random()*10)
        

    def env_rnd(self, op_id):
        op = self.ops[op_id]        
        #op["pitch"]=random.random()*127.0
        #op["volume"]=random.random()
        for i in range(0,len(op["env"])):
            op["env"][i][0]=random.random()
            op["env"][i][1]=random.random()

    def env_max(self, op_id):
        op = self.ops[op_id]
        for i in range(0,len(op["env"])):
            op["env"][i][0]=i/float(len(op["env"]))
            op["env"][i][1]=0.9

    def env_slope(self, op_id):
        op = self.ops[op_id]
        for i in range(0,len(op["env"])):
            op["env"][i][0]=i/float(len(op["env"]))
            op["env"][i][1]=sq(1-i/float(len(op["env"])))

    def env_slopeup(self, op_id):
        op = self.ops[op_id]
        for i in range(0,len(op["env"])):
            op["env"][i][0]=i/float(len(op["env"]))
            op["env"][i][1]=sq(i/float(len(op["env"])))

    def env_tmul(self, op_id, m):
        op = self.ops[op_id]
        for i in range(0,len(op["env"])):
            op["env"][i][0]*=m

    def easy_env(self,op_id,pts):
        op = self.ops[op_id]
        for i in range(0,len(op["env"])):
            op["env"][i][0]=i/float(self.num_ops)
            if i<len(pts):
                op["env"][i][1]=pts[i]
            else:
                op["env"][i][1]=0
        

    def get_sysex(self):
        sysex=[]
        for op in self.ops:
            sysex.append(int(op["pitch"]))
            sysex.append(int(op["volume"]))
            sysex.append(int(op["fixed"]))
            sysex.append(int(op["shape"]))
            for envpt in op["env"]:
                sysex.append(int(envpt[0]*127))
                sysex.append(int(envpt[1]*127))
            for out in op["out"]:
                sysex.append(int(out))
        return sysex

    def from_sysex(self,sysex):
        pos = 0
        for op in self.ops:
            op["pitch"]=sysex[pos]
            pos+=1
            op["volume"]=sysex[pos]
            pos+=1
            op["fixed"]=sysex[pos]
            pos+=1
            op["shape"]=sysex[pos]
            pos+=1
            for envpt in op["env"]:
                envpt[0]=sysex[pos]/127.0
                pos+=1
                envpt[1]=sysex[pos]/127.0
                pos+=1
            for out in range(0, len(op["out"])):
                op["out"][out]=sysex[pos]
                pos+=1

    def pprint(self):
        for op in self.ops:
            self.pprint_op(op)

    def pprint_op(self,op):
        print("pitch="+str(int(op["pitch"])))
        print("volume="+str(int(op["volume"])))
        print("fixed="+str(int(op["fixed"])))
        print("shape="+str(int(op["shape"])))
        for envpt in op["env"]:
            print("t="+str(int(envpt[0]*127))+" v="+str(int(envpt[1]*127)))
        for out in op["out"]:
            print("out="+str(int(out)))


class mutate_win:
    def __init__(self):
        self.midi_ready=True

        # create window
        self.root = Tk()
        self.root.title("mutator "+version)
        top = Frame(self.root)
        top.pack(fill=BOTH)

        self.root.after(500,self.idle)

        self.current_patch=0
        self.patchv = StringVar(self.root)
        self.patchv.set("0")
        menu = OptionMenu(top, self.patchv, command=self.patch_callback, *[str(i) for i in range(16)])
        menu.pack()
        self.current_patch=0

        f=Frame(top)
        f.pack()

        Button(f, text="X", command=lambda: self.play(0)).grid(row=0, column=0)
        Button(f, text="!", command=lambda: self.repop(0)).grid(row=0, column=1)
        Button(f, text="X", command=lambda: self.play(1)).grid(row=1, column=0)
        Button(f, text="!", command=lambda: self.repop(1)).grid(row=1, column=1)
        Button(f, text="X", command=lambda: self.play(2)).grid(row=2, column=0)
        Button(f, text="!", command=lambda: self.repop(2)).grid(row=2, column=1)
        Button(f, text="X", command=lambda: self.play(3)).grid(row=3, column=0)
        Button(f, text="!", command=lambda: self.repop(3)).grid(row=3, column=1)
        Button(f, text="reset", command=self.reset).grid(row=4, column=0) 
        Button(f, text="stop", command=lambda: self.note_off(self.current_patch)).grid(row=4, column=1)
        Button(f, text="load", command=self.load).grid(row=5, column=0)
        Button(f, text="save", command=self.save).grid(row=5, column=1)
    
        self.midiout = rtmidi.MidiOut()
        available_ports = self.midiout.get_ports()

        if available_ports:
            self.midiout.open_port(0)
        else:
            self.midiout.open_virtual_port("My virtual output")

        self.reset()

    def save(self):
        filename = asksaveasfilename(title = "save patch")
        if filename!="":
            with open(filename, 'wb') as output:
                output.write(bytearray(self.patches[self.current_patch].get_sysex()))

    def load(self):
        filename = askopenfilename(title = "load patch")
        if filename!="":
            with open(filename, 'rb') as infile:
                self.patches[0].from_sysex(bytearray(infile.read(90)))

    def patch_callback(self,v):
        self.current_patch=int(self.patchv.get())

    def note_on(self,channel):
        #self.midiout.send_message([0x80|channel,60,0])
        self.midiout.send_message([0x90|channel,60,112])

    def note_off(self,channel):
        self.midiout.send_message([0x80|channel,60,0])

    def idle(self):
        self.midi_ready=True
        #self.tick_seq()
        #self.root.after_idle(self.idle)
        self.root.after(500, self.idle)

    def reset(self):
        self.note_off(self.current_patch)
        self.patches = [patch() for i in range(0,4)]
        for i,p in enumerate(self.patches):
            p.randomise()

    def upload(self,pid):        
        sysex = [0xf0,self.current_patch]+self.patches[pid].get_sysex()
        pad_len = 300-len(sysex)
        self.midiout.send_message(sysex+[0 for i in range(0,pad_len)])
    
    def play(self,pid):
        self.upload(pid)
        self.note_off(self.current_patch)
        self.note_on(self.current_patch)

    def repop(self,pid):
        self.note_off(self.current_patch)
        rate = 0.1
        c = copy.deepcopy(self.patches[pid])
        self.patches[0]=copy.deepcopy(c)
        self.patches[1]=copy.deepcopy(c)
        self.patches[1].mutate(rate)
        self.patches[2]=copy.deepcopy(c)
        self.patches[2].mutate(rate)
        self.patches[3]=copy.deepcopy(c)
        self.patches[3].mutate(rate)
        self.play(0)

    def load_database(self):
        filename = askopenfilename(title = "load database")
        if filename!="":
            pass

    def save_as(self):
        filename = askdirectory(title = "choose a directory to save the csv files in")
        if filename!="":
            for i in get_all_entity_types(self.db,self.table):
                #with open(filename+"/"+i+'.csv','w') as f:
                #    write_csv(f,self.db,self.table,i,False)
                pass

w = mutate_win()

try:
    w.root.mainloop()
except Exception,e:
    print(e)



