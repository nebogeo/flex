import time
import rtmidi
import random
import sys
import platform
import copy
import math

if int(platform.python_version_tuple()[0])>2:
	from tkinter import *
	from tkinter.filedialog import *
	from tkinter.messagebox import *
else:
	from Tkinter import *
	from tkFileDialog import *
	from tkMessageBox import *

version = "0.0.1"

def sq(x): return x*x

class patch:
    def __init__(self):
        self.num_ops=5
        self.num_envpts=6
        self.num_outs=2
        
        self.ops=[]
        for i in range(0,self.num_ops):
            op={}
            op["pitch"]=16
            op["volume"]=127
            op["shape"]=0
            op["fixed"]=0
            op["env"]=[]
            for j in range(0,self.num_envpts):
                op["env"].append([0,1])
            op["out"]=[]
            for j in range(0,self.num_outs):
                op["out"].append(21)
            self.ops.append(op)

    def randomise(self):
        for op in self.ops:
            op["pitch"]=random.random()*127
            op["volume"]=random.random()*127
            op["fixed"]=int(random.random()*2)
            op["shape"]=int(random.random()*7)
            for i in range(0,len(op["env"])):
                op["env"][i][0]=random.random()
                op["env"][i][1]=random.random()
            for i in range(0,len(op["out"])):
                op["out"][i]=int(random.random()*10)

    def mutate(self,rate):
        for op in self.ops:
            if random.random()<rate: op["pitch"]=random.random()*127
            if random.random()<rate: op["volume"]=random.random()*127
            if random.random()<rate: op["fixed"]=int(random.random()*2)
            if random.random()<rate: op["shape"]=int(random.random()*7)
            for i in range(0,len(op["env"])):
                if random.random()<rate: op["env"][i][0]=random.random()
                if random.random()<rate: op["env"][i][1]=random.random()
            for i in range(0,len(op["out"])):
                if random.random()<rate: op["out"][i]=int(random.random()*10)
        

    def env_rnd(self, op_id):
        op = self.ops[op_id]        
        #op["pitch"]=random.random()*127.0
        #op["volume"]=random.random()
        for i in range(0,len(op["env"])):
            op["env"][i][0]=random.random()
            op["env"][i][1]=random.random()

    def env_max(self, op_id):
        op = self.ops[op_id]
        for i in range(0,len(op["env"])):
            op["env"][i][0]=i/float(len(op["env"]))
            op["env"][i][1]=0.9

    def env_slope(self, op_id):
        op = self.ops[op_id]
        for i in range(0,len(op["env"])):
            op["env"][i][0]=i/float(len(op["env"]))
            op["env"][i][1]=sq(1-i/float(len(op["env"])))

    def env_slopeup(self, op_id):
        op = self.ops[op_id]
        for i in range(0,len(op["env"])):
            op["env"][i][0]=i/float(len(op["env"]))
            op["env"][i][1]=sq(i/float(len(op["env"])))

    def env_slopeupdown(self, op_id):
        op = self.ops[op_id]
        for i in range(0,len(op["env"])):
            op["env"][i][0]=i/float(len(op["env"]))
            op["env"][i][1]=math.sin(i/float(len(op["env"])-1)*3.141)

    def env_tmul(self, op_id, m):
        op = self.ops[op_id]
        for i in range(0,len(op["env"])):
            op["env"][i][0]*=m

    def env_tadd(self, op_id, m):
        op = self.ops[op_id]
        for i in range(1,len(op["env"])):
            op["env"][i][0]+=m

    def easy_env(self,op_id,pts):
        op = self.ops[op_id]
        for i in range(0,len(op["env"])):
            op["env"][i][0]=i/float(self.num_ops)
            if i<len(pts):
                op["env"][i][1]=pts[i]
            else:
                op["env"][i][1]=0
        

    def get_sysex(self):
        sysex=[]
        for op in self.ops:
            sysex.append(int(op["pitch"]))
            sysex.append(int(op["volume"]))
            sysex.append(int(op["fixed"]))
            sysex.append(int(op["shape"]))
            for envpt in op["env"]:
                sysex.append(int(envpt[0]*127))
                sysex.append(int(envpt[1]*127))
            for out in op["out"]:
                sysex.append(int(out))
        return sysex

    def from_sysex(self,sysex):
        pos = 0
        for op in self.ops:
            op["pitch"]=sysex[pos]
            pos+=1
            op["volume"]=sysex[pos]
            pos+=1
            op["fixed"]=sysex[pos]
            pos+=1
            op["shape"]=sysex[pos]
            pos+=1
            for envpt in op["env"]:
                envpt[0]=sysex[pos]/127.0
                pos+=1
                envpt[1]=sysex[pos]/127.0
                pos+=1
            for out in range(0, len(op["out"])):
                op["out"][out]=sysex[pos]
                pos+=1

    def pprint(self):
        for op in self.ops:
            self.pprint_op(op)

    def pprint_op(self,op):
        print("pitch="+str(int(op["pitch"])))
        print("volume="+str(int(op["volume"])))
        print("fixed="+str(int(op["fixed"])))
        print("shape="+str(int(op["shape"])))
        for envpt in op["env"]:
            print("t="+str(int(envpt[0]*127))+" v="+str(int(envpt[1]*127)))
        for out in op["out"]:
            print("out="+str(int(out)))

############################################################

class env_canvas:
    def __init__(self,master,y,x):
        self.canvas = Canvas(master, width=200, height=25)
        self.canvas.grid(row=y, column=x)

    def set(self,env):
        self.canvas.delete(ALL) # remove all items
        pts=[]
        for e in env:
            pts.append(e)
        pts=sorted(pts, key=lambda p: p[0])
        pts[0][0]=0
        lx=0
        ly=0
        for e in pts:
            self.canvas.create_line(lx, ly, e[0]*200, 25-e[1]*25)
            lx=e[0]*200
            ly=25-e[1]*25

class win:
    def __init__(self):
        self.controls=[]
        self.patches = [patch() for i in range(0,4)]
        self.patch = self.patches[0]
        self.midi_ready=True

        for i in range(0,self.patch.num_ops):
            self.controls.append({})

        # create window
        self.root = Tk()
        self.root.title("flex7 "+version)
        top = Frame(self.root)
        top.pack(fill=BOTH)

        self.root.after(500,self.idle)

        self.current_patch=0
        self.patchv = StringVar(self.root)
        self.patchv.set("0")
        menu = OptionMenu(top, self.patchv, command=self.patch_callback, *[str(i) for i in range(4)])
        menu.pack()

        f=Frame(top)
        f.pack()

        self.title(f,1,"pitch")
        self.title(f,3,"volume")
        self.title(f,4,"note freq/vol")
        self.title(f,5,"shape")
        self.title(f,6,"out1")
        self.title(f,7,"out2")
        #for i in range(0,12):
        #    self.title(f,6+i,"env"+str(i))

        for i in range(0,self.patch.num_ops):
            self.op_params(i,f)

        Button(f, text="X", command=self.play).grid(row=21, column=0)
        Button(f, text="-", command=self.stop).grid(row=21, column=1)
        Button(f, text="?", command=self.rnd).grid(row=21, column=2)
        Button(f, text="load", command=self.load).grid(row=21, column=3)
        Button(f, text="save", command=self.save).grid(row=21, column=4)

        f=Frame(top)
        f.pack()

        # self.seq = []
        # for c in range(0,6):
        #     seq = []
        #     for i in range(0,32):
        #         seq.append(IntVar())
        #         ch = Checkbutton(f, text="", variable=seq[i])
        #         ch.grid(row=c, column=i)
        #     self.seq.append(seq)

        # self.seq_pos=0
            
        self.midiout = rtmidi.MidiOut()
        available_ports = self.midiout.get_ports()
        print(available_ports)
        if available_ports:
            self.midiout.open_port(0)
        else:
            self.midiout.open_virtual_port("My virtual output")

    def save(self):
        filename = asksaveasfilename(title = "save patch")
        if filename!="":
            with open(filename, 'wb') as output:
                    output.write(bytearray(self.patch.get_sysex()))

    def load(self):
        filename = askopenfilename(title = "load patch")
        if filename!="":
            with open(filename, 'rb') as infile:
                #for p in range(0,len(self.patches)):
                self.patch.from_sysex(bytearray(infile.read(90)))
        self.read_patch()

    def note_on(self,channel):
        self.midiout.send_message([0x80|channel,60,0])
        self.midiout.send_message([0x90|channel,60,112])

    def patch_callback(self,v):
        self.current_patch=int(self.patchv.get())
        self.patch = self.patches[self.current_patch]
        self.read_patch()

    def idle(self):
        self.midi_ready=True
        #self.tick_seq()
        #self.root.after_idle(self.idle)
        self.root.after(500, self.idle)

    def tick_seq(self):
        for c in range(0,6):
            if self.seq[c][self.seq_pos].get():
                self.note_on(c)
        self.seq_pos+=1
        self.seq_pos=self.seq_pos%32

    def rnd(self):
        self.patch.randomise()
        self.read_patch()
        self.play()

    def stop(self):
        pass

    def play(self):
        self.write_patch()
        self.note_on(self.current_patch)

    def op_params(self,i,f):
        Label(f, text=str(i)).grid(row=i+1,column=0)
        #self.float_param(f,i,1,"pitch",440)
        self.float_param(f,i,1,"pitch",16)
        #self.controls[i]["pitch"]=Scale(f, from_=0, to=127, orient=HORIZONTAL, command=self.update)
        self.controls[i]["pitch"].grid(row=i+1,column=1)
        self.controls[i]["pitch_out"]=StringVar()
        Label(f, textvariable=self.controls[i]["pitch_out"]).grid(row=i+1,column=2)
        
        self.controls[i]["volume"]=Scale(f, from_=0, to=127, orient=HORIZONTAL, command=self.update)
        self.controls[i]["volume"].grid(row=i+1,column=3)
        self.controls[i]["fixed"] = IntVar()
        Checkbutton(f, text="fix", variable=self.controls[i]["fixed"]).grid(row=i+1,column=4)        
        self.controls[i]["shape"] = StringVar(self.root)
        self.controls[i]["shape"] .set("sine") # default value
        OptionMenu(f, self.controls[i]["shape"], "sine", "square", "saw", "revsaw", "tri", "pulse1", "pulse2").grid(row=i+1,column=5)
        self.float_param(f,i,6,"out0",21)
        self.float_param(f,i,7,"out1",21)

        Button(f, text="?", command=lambda: self.env_rnd(i)).grid(row=i+1, column=8)
        Button(f, text="m", command=lambda: self.env_max(i)).grid(row=i+1, column=9)
        Button(f, text="\\", command=lambda: self.env_slope(i)).grid(row=i+1, column=10)
        Button(f, text="/", command=lambda: self.env_slopeup(i)).grid(row=i+1, column=11)
        Button(f, text="n", command=lambda: self.env_slopeupdown(i)).grid(row=i+1, column=12)
        Button(f, text="+", command=lambda: self.env_tmul(i,1.1)).grid(row=i+1, column=13)
        Button(f, text="-", command=lambda: self.env_tmul(i,0.9)).grid(row=i+1, column=14)
        Button(f, text=">", command=lambda: self.env_tadd(i,0.1)).grid(row=i+1, column=15)
        Button(f, text="<", command=lambda: self.env_tadd(i,-0.1)).grid(row=i+1, column=16)

        self.controls[i]["env"]=env_canvas(f,i+1,17)
        
        #for j in range(0,12):
        #    self.float_param(f,i,6+j,"env"+str(i))

    def title(self,f,pos,name):
        Label(f, text=name).grid(row=0,column=pos)

    def float_param(self,root,posy,pos,name,df):
        self.root.bind('<Return>', self.update)
        entry = Entry(root, width=5)
        entry.grid(row=posy+1, column=pos)
        entry.delete(0, END)
        entry.insert(0, str(df))
        self.controls[posy][name]=entry

    def update(self,v):
        self.write_patch()
        self.read_patch()

    def env_max(self,op_id):
        self.write_patch()
        self.patch.env_max(op_id)
        self.read_patch()

    def env_rnd(self,op_id):
        self.write_patch()
        self.patch.env_rnd(op_id)
        self.read_patch()

    def env_slope(self,op_id):
        self.write_patch()
        self.patch.env_slope(op_id)
        self.read_patch()

    def env_slopeup(self,op_id):
        self.write_patch()
        self.patch.env_slopeup(op_id)
        self.read_patch()

    def env_slopeupdown(self,op_id):
        self.write_patch()
        self.patch.env_slopeupdown(op_id)
        self.read_patch()

    def env_tmul(self,op_id,m):
        self.write_patch()
        self.patch.env_tmul(op_id,m)
        self.read_patch()

    def env_tadd(self,op_id,m):
        self.write_patch()
        self.patch.env_tadd(op_id,m)
        self.read_patch()

    def read_patch(self):
        shapes = ["sine","square","saw","revsaw","triangle","pulse1","pulse2"]
        for i,control in enumerate(self.controls):
            #control["pitch"].set(self.patch.ops[i]["pitch"])
            control["pitch"].delete(0, END)
            control["pitch"].insert(0,str(int(self.patch.ops[i]["pitch"])))
            
            control["pitch_out"].set(self.patch.ops[i]["pitch"]/16.0)
            control["volume"].set(self.patch.ops[i]["volume"])
            control["fixed"].set(self.patch.ops[i]["fixed"])
            control["shape"].set(shapes[self.patch.ops[i]["shape"]])
            control["env"].set(self.patch.ops[i]["env"])
            for j in range(0,self.patch.num_outs):
                control["out"+str(j)].delete(0, END)
                control["out"+str(j)].insert(0,str(int(self.patch.ops[i]["out"][j]%64)))


    def write_patch(self):
        self._write_patch(self.patch)

    def _write_patch(self,patch):
        shapes = {"sine":0,"square":1,"saw":2,"revsaw":3,"triangle":4,"pulse1":5,"pulse2":6}
        for i,control in enumerate(self.controls):
            #patch.ops[i]["pitch"]=control["pitch"].get()
            patch.ops[i]["pitch"]=int(control["pitch"].get())
            patch.ops[i]["volume"]=control["volume"].get()
            patch.ops[i]["fixed"]=control["fixed"].get()
            patch.ops[i]["shape"]=shapes[control["shape"].get()]
            for j in range(0,patch.num_outs):
                patch.ops[i]["out"][j]=float(control["out"+str(j)].get())

        if self.midi_ready:
            sysex = [0xf0,self.current_patch]+patch.get_sysex()
            pad_len = 300-len(sysex)
            self.midiout.send_message(sysex+[0 for i in range(0,pad_len)])
            self.midi_ready=False

    def load_database(self):
        filename = askopenfilename(title = "load database")
        if filename!="":
            pass

    def save_as(self):
        filename = askdirectory(title = "choose a directory to save the csv files in")
        if filename!="":
            for i in get_all_entity_types(self.db,self.table):
                #with open(filename+"/"+i+'.csv','w') as f:
                #    write_csv(f,self.db,self.table,i,False)
                pass

#############################################################

class mutate_win:
    def __init__(self):
        self.midi_ready=True

        # create window
        self.root = Tk()
        self.root.title("flex "+version)
        top = Frame(self.root)
        top.pack(fill=BOTH)

        self.root.after(500,self.idle)

        self.current_patch=0
        self.patchv = StringVar(self.root)
        self.patchv.set("0")
        menu = OptionMenu(top, self.patchv, command=self.patch_callback, *[str(i) for i in range(16)])
        menu.pack()
        self.current_patch=0

        f=Frame(top)
        f.pack()

        Button(f, text="X", command=lambda: self.play(0)).grid(row=0, column=0)
        Button(f, text="!", command=lambda: self.repop(0)).grid(row=0, column=1)
        Button(f, text="X", command=lambda: self.play(1)).grid(row=1, column=0)
        Button(f, text="!", command=lambda: self.repop(1)).grid(row=1, column=1)
        Button(f, text="X", command=lambda: self.play(2)).grid(row=2, column=0)
        Button(f, text="!", command=lambda: self.repop(2)).grid(row=2, column=1)
        Button(f, text="X", command=lambda: self.play(3)).grid(row=3, column=0)
        Button(f, text="!", command=lambda: self.repop(3)).grid(row=3, column=1)
        Button(f, text="reset", command=self.reset).grid(row=4, column=0) 
        Button(f, text="stop", command=lambda: self.note_off(self.current_patch)).grid(row=4, column=1)
    
        self.midiout = rtmidi.MidiOut()
        available_ports = self.midiout.get_ports()

        if available_ports:
            self.midiout.open_port(0)
        else:
            self.midiout.open_virtual_port("My virtual output")

        self.reset()

    def patch_callback(self,v):
        self.current_patch=int(self.patchv.get())

    def note_on(self,channel):
        #self.midiout.send_message([0x80|channel,60,0])
        self.midiout.send_message([0x90|channel,60,112])

    def note_off(self,channel):
        self.midiout.send_message([0x80|channel,60,0])

    def idle(self):
        self.midi_ready=True
        #self.tick_seq()
        #self.root.after_idle(self.idle)
        self.root.after(500, self.idle)

    def reset(self):
        self.note_off(self.current_patch)
        self.patches = [patch() for i in range(0,4)]
        for i,p in enumerate(self.patches):
            p.randomise()

    def upload(self,pid):        
        sysex = [0xf0,self.current_patch]+self.patches[pid].get_sysex()
        pad_len = 300-len(sysex)
        self.midiout.send_message(sysex+[0 for i in range(0,pad_len)])
    
    def play(self,pid):
        self.upload(pid)
        self.note_off(self.current_patch)
        self.note_on(self.current_patch)

    def repop(self,pid):
        self.note_off(self.current_patch)
        rate = 0.05
        c = copy.deepcopy(self.patches[pid])
        self.patches[0]=copy.deepcopy(c)
        self.patches[1]=copy.deepcopy(c)
        self.patches[1].mutate(rate)
        self.patches[2]=copy.deepcopy(c)
        self.patches[2].mutate(rate)
        self.patches[3]=copy.deepcopy(c)
        self.patches[3].mutate(rate)
        self.play(0)

    def load_database(self):
        filename = askopenfilename(title = "load database")
        if filename!="":
            pass

    def save_as(self):
        filename = askdirectory(title = "choose a directory to save the csv files in")
        if filename!="":
            for i in get_all_entity_types(self.db,self.table):
                #with open(filename+"/"+i+'.csv','w') as f:
                #    write_csv(f,self.db,self.table,i,False)
                pass

w = win()

try:
    w.root.mainloop()
except Exception,e:
    print(e)



