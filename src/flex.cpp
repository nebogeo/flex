// Copyright (C) 2016 FoAM Kernow (kernow@fo.am)
//
// this program is free software; you can redistribute it and/or modify
// it under the terms of the GNU general public license as published by
// the free software foundation; either version 2 of the license, or
// (at your option) any later version.
//
// this program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  see the
// GNU general public license for more details.
//
// you should have received a copy of the GNU general public license
// along with this program; if not, write to the free software
// foundation, inc., 59 temple place - suite 330, boston, MA 02111-1307, USA.

#include <algorithm>
#include "flex.h"

using namespace spiralcore;

#define MAX_BUFFER 4096
#define GLOBAL_GAIN 0.2f

flex::op::op(u32 sr, u32 id, u32 num_env_points, u32 num_outputs) :
  m_id(id),
  m_input_buffer(MAX_BUFFER),
  m_output_buffer(MAX_BUFFER),
  m_pm_op(sr, num_env_points),
  m_ran(false)
{
  for (u32 i=0; i<num_outputs; i++) {
    m_output_ids.push_back(999);
    m_output_level.push_back(0);
  }
}

void flex::op::trigger(float freq, float vol) { 
  m_pm_op.trigger(freq,vol); 
}

void flex::op::stop() { 
  m_pm_op.stop(); 
}

flex::flex(u32 sr, u32 num_ops, u32 num_env_points, u32 num_op_outputs):
  m_buffer(MAX_BUFFER),
  m_num_env_points(num_env_points),
  m_num_op_outputs(num_op_outputs)
{
  pm_op::init_table();

  for (u32 i=0; i<num_ops; i++) {
    m_ops.push_back(op(sr,i,num_env_points,num_op_outputs));
  }
  
  m_ops[0].set_pitch(1);
  m_ops[0].set_volume(1);
  m_ops[0].set_env(0,0,1);
  m_ops[0].set_env(1,5,0);
  m_ops[0].set_env(2,10,0);
  m_ops[0].set_env(3,10,0);
  //m_ops[0].connect_output(0,99);
 
  m_ops[1].set_pitch(2);
  m_ops[1].set_fixed(true);
  m_ops[1].set_volume(1);
  m_ops[1].set_env(0,0,1);
  m_ops[1].set_env(1,5,0);
  m_ops[1].connect_output(0,0,1); 

  //m_ops[2].set_pitch(100);
  //m_ops[2].set_volume(0.2);
  //m_ops[2].connect_output(0,1,1); 
  //m_ops[2].connect_output(1,2,1); 
  
}

void flex::trigger(float freq, float vol) {
  for (auto &o:m_ops) {
    o.trigger(freq,vol);
  }
}

void flex::stop() {
  for (auto &o:m_ops) {
    o.stop();
  }
}

// ops*(3+envp*2+opouts)+3 (600 bytes)

void flex::load_sysex(u8* data) {
  u32 pos=3;
  for (auto &o:m_ops) {
    o.set_pitch(data[pos++]/16.0); // ~ 0->8X 
    o.set_volume(data[pos++]/128.0);
    o.set_fixed(data[pos++]);
    o.set_shape(data[pos++]%7);

    // load in envelope data points
    float MAX_ENV=5.0f;
    vector<pair<float,float>> points;
    for (u32 i=0; i<m_num_env_points; i++) {
      float t=(data[pos++]/127.0f)*MAX_ENV;
      points.push_back(pair<float,float>(t,data[pos++]/127.0f));
    }
    
    // sort into time order
    sort(points.begin(), points.end(), [](const pair<float,float> &left, const pair<float,float> &right) {
	return left.first < right.first;
      });
    
    // snap to start
    points[0].first=0;

    u32 c=0;
    for (auto &p:points) {
      o.set_env(c++,p.first,p.second);
    }

    for(u32 i=0; i<m_num_op_outputs; i++) {
      o.connect_output(i,data[pos++]%64,1);//data[pos++]/127.0f);
    }
  }
}

void flex::recur(u32 id, u32 size) {
  if (id>=m_ops.size()) return;

  // clear input buffer
  m_ops[id].m_input_buffer.zero();

  // run all inputs, collecting output buffers
  bool has_input=false;

  // search for ops that output to this op, and run them
  for (auto &o:m_ops) {
    u32 output=0;
    // check each output id
    for (auto out_id:o.m_output_ids) {
      // does it send to us?
      if (id==out_id) {
	has_input=true;
	// don't recurse to ourselves (but turn feedback on)
	if (!o.m_ran) {
	  o.m_ran=true;
	  recur(o.m_id,size);
	  // read output level from the sender
	  m_ops[id].m_input_buffer.mul_mix(o.m_output_buffer,0,
					   o.m_output_level[output]);
	} else {
	  m_ops[id].set_feedback(true);
	}
	
      }
      output++;
    }
   }
  
  if (has_input) {
    m_ops[id].m_pm_op.run(m_ops[id].m_output_buffer,
			  &m_ops[id].m_input_buffer,
			  size);
  } else {
    m_ops[id].m_pm_op.run(m_ops[id].m_output_buffer, NULL, size);
  }
  m_ops[id].set_feedback(false);

}

void flex::run(float *output, u32 size) {  
  for (auto &o:m_ops) {
    o.m_ran=false;
  }

  recur(0,size);
    
  for (u32 i=0; i<size; i++) {
    output[i]+=m_ops[0].m_output_buffer[i]*GLOBAL_GAIN;
    //if (i==0) cerr<<output[i]<<endl;
  }
}
 


