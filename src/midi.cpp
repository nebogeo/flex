#include <stdio.h>
#include <portmidi.h>
#include "midi.h"

#include <iostream>

using namespace spiralcore;
using namespace std;

void midi::init() {
  Pm_Initialize();

  /* list device information */
  for (int i = 0; i < Pm_CountDevices(); i++) {
    const PmDeviceInfo *info = Pm_GetDeviceInfo(i);
    printf("%d: %s, %s", i, info->interf, info->name);
    if (info->input) printf(" (input)");
    if (info->output) printf(" (output)");
    printf("\n");
  }

}

midi::midi(bool output, u32 device) {
  if (output) {
    const PmDeviceInfo *info = Pm_GetDeviceInfo(device);
    if (info == NULL) {
      printf("Could not open output device (%d).", device);
      return;
    }
    printf("Opening output device (%d) %s %s\n", device, info->interf, info->name);
    
    Pm_OpenOutput(&midi_out, device, NULL, 0, NULL, NULL,0);
  } else {
    const PmDeviceInfo *info = Pm_GetDeviceInfo(device);
    printf("Opening input device (%d)\n", device);    
    Pm_OpenInput(&midi_in, device, NULL, 512, NULL, NULL);
  }
}


void midi::send(u8* data, unsigned int len) {
  PmEvent msg;
  msg.timestamp = 0;
  msg.message = 0;
  unsigned int shift = 0;
  unsigned int pos = 0;

  while (pos<len) {
    msg.message |= (data[pos] << shift);
    //printf("msg: %x data[pos]: %x \n",msg.message,data[pos]);
    shift += 8;
    pos++;
    
    if (shift==32 || pos==len) {
      //printf("sending...%x\n", msg.message);
      Pm_Write(midi_out, &msg, 1);
      msg.message = 0;
      shift = 0;
    }
  }
}

void midi::receive(u8* receive_data, unsigned int len) {
  int pos=0;
  PmEvent event;
  int shift = 0;
  bool first=true;
  u32 size=3;
  u8 data=0;

  int count = Pm_Read(midi_in, &event, 1);
  if (count==0) return;

  data = event.message & 0xFF;
  if (data!=0xF0) {
    for (shift = 0; shift < 32; shift += 8) {
      receive_data[pos++] = (event.message>>shift) & 0xFF;
      if (pos>=size || pos>=len) {
	return;
      }
    }

  } else {
    receive_data[pos++] = data;
    while (true) {
      for (shift = 0; shift < 32; shift += 8) {
	receive_data[pos++] = (event.message>>shift) & 0xFF;
	if (pos>=len || receive_data[pos-1]==0xF7) {
	  return;
	}
      }

      int count = Pm_Read(midi_in, &event, 1);
      if (count == 0) {
	return;
      }
      //cerr<<count<<endl;
    }
  }
}









