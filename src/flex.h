// Copyright (C) 2016 FoAM Kernow (kernow@fo.am)
//
// this program is free software; you can redistribute it and/or modify
// it under the terms of the GNU general public license as published by
// the free software foundation; either version 2 of the license, or
// (at your option) any later version.
//
// this program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  see the
// GNU general public license for more details.
//
// you should have received a copy of the GNU general public license
// along with this program; if not, write to the free software
// foundation, inc., 59 temple place - suite 330, boston, MA 02111-1307, USA.

#include "pm_op.h"

#ifndef FLEX
#define FLEX

namespace spiralcore {

  class flex {
  public:
    flex(u32 sr, u32 num_ops, u32 num_env_points, u32 num_op_outputs);
    void run(float *output, u32 size);
    void trigger(float freq, float vol);
    void stop();
    void load_sysex(u8* data);

    void recur(u32 id, u32 size);

    class op {
    public:
      op(u32 sr, u32 id, u32 num_env_points, u32 num_outputs);
      void trigger(float freq, float vol);
      void stop();
      void set_env(u32 n, float time, float level) { m_pm_op.m_amp_env.set_point(n,time,level); }
      void connect_output(u32 n, u32 id, float level) { m_output_ids[n]=id; m_output_level[n]=level; }
      void set_pitch(float v) { m_pm_op.m_pitch_mult=v; }
      void set_volume(float v) { m_pm_op.m_base_volume=v; }
      void set_shape(u32 v) { m_pm_op.m_waveshape=v; }
      void set_fixed(bool v) { m_pm_op.m_fixed=v; }
      void set_feedback(bool v) { m_pm_op.m_feedback=v; }
      
      u32 m_id;
      sample m_input_buffer;
      sample m_output_buffer;
      pm_op m_pm_op;
      vector<u32> m_output_ids;
      vector<u32> m_output_level;
      bool m_ran;
    };
   
  private:
    vector<op> m_ops;
    sample m_buffer;
    u32 m_num_env_points;
    u32 m_num_op_outputs;
  };
  
};

#endif
