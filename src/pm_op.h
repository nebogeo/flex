// Copyright (C) 2016 FoAM Kernow (kernow@fo.am)
//
// this program is free software; you can redistribute it and/or modify
// it under the terms of the GNU general public license as published by
// the free software foundation; either version 2 of the license, or
// (at your option) any later version.
//
// this program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  see the
// GNU general public license for more details.
//
// you should have received a copy of the GNU general public license
// along with this program; if not, write to the free software
// foundation, inc., 59 temple place - suite 330, boston, MA 02111-1307, USA.

#include "jellyfish/sample.h"

#ifndef PM_OP
#define PM_OP

namespace spiralcore {
  
  static const int NUM_TABLES = 9;

  class pm_op {
  public:
    static void init_table();  
    pm_op(u32 sr, u32 num_env_points);
    void trigger(float freq, float vol);
    void stop();
    void run(sample &out, sample *in, u32 size);

    class env {
    public:
      env(u32 size);
      void set_point(u32 n, float time, float level);
      float value(float &t);  
    private:
      // time, level
      vector<pair<float,float>> m_env;
    };

    env m_amp_env;
    float m_note_freq;
    float m_note_vol;
    float m_pitch_mult;
    float m_base_volume;
    u32 m_waveshape;
    bool m_fixed;
    bool m_feedback;


  private:
    enum table_shape {SINE,SQUARE,SAW,REVSAW,TRIANGLE,PULSE1,PULSE2};
    static const u32 TABLE_SIZE;
    static sample m_table[NUM_TABLES];
    float m_cycle_pos;
    float m_t;
    u32 m_sample_rate;
    bool m_running;
    float m_feedback_sample;
};

}

#endif
