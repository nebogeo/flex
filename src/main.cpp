#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <assert.h>
#include <stdlib.h>
#include <pthread.h>
#include "flex.h"
#include "midi.h"
#include "JackClient.h"

using namespace spiralcore;
using namespace std;

float *m_jack_buffer;
pthread_mutex_t* m_mutex;
static const u32 VOICES=4;
static const u32 POLYPHONY=8;

sample compare(44100);

void jack_callback(void *context, unsigned int size) {
  flex **f=static_cast<flex**>(context);

  for (u32 i=0; i<size; i++) {
    m_jack_buffer[i]=0;
  }

  if (!pthread_mutex_trylock(m_mutex)) {
    for (u32 d=0; d<VOICES*POLYPHONY; d++) {
      f[d]->run(m_jack_buffer,size);
    }
    pthread_mutex_unlock(m_mutex);
  } 
}


void connect_jack(flex **f) {
  u32 BufferLength=4096;
  m_jack_buffer = new float[BufferLength];
  
  int temp=0;
  JackClient *Jack = JackClient::Get();
  Jack->SetCallback(jack_callback,f);
  Jack->Attach("flex");
  if (Jack->IsAttached()) {	
    int id=Jack->AddOutputPort();
    Jack->SetOutputBuf(id,m_jack_buffer);
    Jack->ConnectOutput(id, "system:playback_1");
    //Jack->ConnectInput(id, "hexter DX7 emulation (v1.0.3):hexter DX7 emulation (v1.0.3) out_1");
    cerr<<"Attached to jack"<<endl;
  } else {
    cerr<<"Could not attach to jack"<<endl;
  }
}

int main(int argc, char *argv[]) {
  midi::init();
  srand(time(NULL));

  float midi_note[127];
  float a = 440.0f; // a is 440 hz...
  for (int x = 0; x < 127; ++x) {
    midi_note[x] = (a / 32.0f) * powf(2.0f,((x - 9.0f) / 12.0f));
  }

  m_mutex = new pthread_mutex_t;
  pthread_mutex_init(m_mutex,NULL);

  // I IZ CRAZY C DUDE CAN DO EVERYTHING WITH ARRAYS

  // for finding notes for note off event
  int polyslot_note[POLYPHONY*VOICES];
  for (u32 d=0; d<POLYPHONY*VOICES; d++) {
    polyslot_note[d]=0;
  }
  
  // keeping track of next note to trigger
  int cur_poly[POLYPHONY];
  for (u32 d=0; d<POLYPHONY; d++) {
    cur_poly[d]=0;
  }

  // the voices
  flex *f[VOICES*POLYPHONY];
  midi input_midi(false,1);
  for (u32 d=0; d<VOICES*POLYPHONY; d++) {
    f[d]=new flex(44100,5,6,2);
  }

  connect_jack(&f[0]);

  u8 midi_data[300];

  while(1) {
    midi_data[0]=0;
    input_midi.receive(midi_data,300);
    if (midi_data[0]!=0) {
      //cerr<<"midi: "<<(int)midi_data[0]<<" "<<
      //	(int)midi_data[1]<<" "<<
      //	(int)midi_data[2]<<endl;
      u8 midi_type = midi_data[0]>>4;
      u8 midi_chan = midi_data[0]&0x0f;

      //cerr<<(int)midi_data[0]<<" "<<(int)midi_type<<" "<<(int)midi_chan<<endl;
      
      switch(midi_type) {
      case 0x9: {
	int voice = midi_chan%VOICES;
	// increment the polyphony
	cur_poly[voice]=(cur_poly[voice]+1)%POLYPHONY;
	// get the slot to play
	int slot = voice*POLYPHONY+cur_poly[voice];
	// record the midi note for note off
	polyslot_note[slot]=midi_data[1];
      
	pthread_mutex_lock(m_mutex);
	f[slot]->trigger(midi_note[midi_data[1]],
			 midi_data[2]/127.0f);
	pthread_mutex_unlock(m_mutex);
      } break;
      case 0x8: {
	int voice = midi_chan%VOICES;
	int slot=-1;
	for (u32 i=0; i<POLYPHONY; i++) {
	  if (polyslot_note[voice*POLYPHONY+i]==midi_data[1]) {
	    slot=voice*POLYPHONY+i;
	  }	      
	}
	
	if (slot!=-1) {
	  pthread_mutex_lock(m_mutex);
	  f[slot]->stop();
	  pthread_mutex_unlock(m_mutex);
	} else {
	  //cerr<<"can't find note on for note off "<<midi_data[1]<<endl;
	}
      } break;
      case 0xf: {
	pthread_mutex_lock(m_mutex);
	int voice = midi_data[2]%VOICES;
	for (u32 i=0; i<POLYPHONY; i++) {
	  f[voice*POLYPHONY+i]->load_sysex(midi_data);
	}
	pthread_mutex_unlock(m_mutex);
      } break;
      }
    }
    usleep(1000);
  }
  
  cerr<<"complete"<<endl;
 
  return 0;
}
