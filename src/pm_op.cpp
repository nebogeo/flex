// Copyright (C) 2016 FoAM Kernow (kernow@fo.am)
//
// this program is free software; you can redistribute it and/or modify
// it under the terms of the GNU general public license as published by
// the free software foundation; either version 2 of the license, or
// (at your option) any later version.
//
// this program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  see the
// GNU general public license for more details.
//
// you should have received a copy of the GNU general public license
// along with this program; if not, write to the free software
// foundation, inc., 59 temple place - suite 330, boston, MA 02111-1307, USA.

#include "pm_op.h"
#include <iostream>

using namespace spiralcore;
using namespace std;

const u32 pm_op::TABLE_SIZE=4096;
static const int NUM_TABLES = 7;
sample pm_op::m_table[NUM_TABLES];

// should be a parameter...
static const float FEEDBACK_GAIN=0.3;

void pm_op::init_table() {
  for (int n=0; n<NUM_TABLES; n++) {
    m_table[n].allocate(TABLE_SIZE);
  }
  
  float rad_cycle = (M_PI/180)*360;
  float pos=0;

  for (unsigned int n=0; n<TABLE_SIZE; n++) {
    if (n==0) pos=0;
    else pos=(n/(float)TABLE_SIZE)*rad_cycle;
    m_table[SINE].set(n,sin(pos));
  }

  for (unsigned int n=0; n<TABLE_SIZE; n++) {
    if (n<TABLE_SIZE/2) m_table[SQUARE].set(n,1.0f);
    else m_table[SQUARE].set(n,-1);
  }

  for (unsigned int n=0; n<TABLE_SIZE; n++) {
    m_table[REVSAW].set(n,((n/(float)TABLE_SIZE)*2.0f)-1.0f);
  }

  for (unsigned int n=0; n<TABLE_SIZE; n++) {
    m_table[SAW].set(n,1-(n/(float)TABLE_SIZE)*2.0f);
  }

  float half_tab=TABLE_SIZE/2;
  float v=0;
  for (unsigned int n=0; n<TABLE_SIZE; n++) {
    if (n<half_tab) v=1-(n/half_tab)*2.0f;
    else v=(((n-half_tab)/half_tab)*2.0f)-1.0f;
    v*=0.99;
    m_table[TRIANGLE].set(n,v);
  }

  for (unsigned int n=0; n<TABLE_SIZE; n++) {
    if (n<TABLE_SIZE/1.2) m_table[PULSE1].set(n,1);
    else m_table[PULSE1].set(n,-1);
  }
  
  for (unsigned int n=0; n<TABLE_SIZE; n++) {
    if (n<TABLE_SIZE/1.5) m_table[PULSE2].set(n,1);
    else m_table[PULSE2].set(n,-1);
  }
}

pm_op::pm_op(u32 sr, u32 num_env_points) :
  m_cycle_pos(0),
  m_t(1000),
  m_pitch_mult(1),
  m_note_freq(0),
  m_base_volume(0.1),
  m_amp_env(num_env_points),
  m_sample_rate(sr),
  m_waveshape(0),
  m_fixed(false),
  m_feedback(false),
  m_running(false),
  m_feedback_sample(0)
{
}

void pm_op::trigger(float freq, float vol) { 
  m_note_freq=freq;
  m_note_vol=vol;
  m_t=0; 
  m_running=true;
}

void pm_op::stop() { 
  m_running=false;
}

float rmod(float a,float b) {
  return a-b*floor(a/b);
}

void pm_op::run(sample &out, sample *in, u32 size) {
  if (!m_running) {
    out.zero();
    return;
  }
  float base_freq=m_note_freq;
  if (m_fixed) base_freq=440.0;
  float incr = (base_freq*m_pitch_mult)*(TABLE_SIZE/(float)m_sample_rate);
  float sample_len = 1/(float)m_sample_rate;
  float pos = 0;

  for (u32 n=0; n<size; n++) {
    m_cycle_pos+=incr;

    m_cycle_pos=fmod(m_cycle_pos,TABLE_SIZE-1);
    pos = m_cycle_pos;
    if (m_feedback) pos+=m_feedback_sample*TABLE_SIZE*FEEDBACK_GAIN;

    if (in!=NULL) {
      pos+=((*in)[n]*TABLE_SIZE);
      pos=rmod(pos,TABLE_SIZE-1);
    }

    float vol = m_base_volume;
    if (!m_fixed) vol*=m_note_vol;
    
    out[n]=m_table[m_waveshape][pos]*vol*m_amp_env.value(m_t);
    m_feedback_sample=out[n];
    m_t+=sample_len;
  }  
}

pm_op::env::env(u32 size) {
  for (u32 i=0; i<size; i++) {
    m_env.push_back(pair<float,float>(0.5,1.0));
  } 
}

void pm_op::env::set_point(u32 n, float time, float level) { 
  m_env[n].first=time;
  m_env[n].second=level;
}

float pm_op::env::value(float &t) {
  // get points either side of t
  for (u32 i=1; i<m_env.size(); i++) {
    if (t>m_env[i-1].first && t<=m_env[i].first) {
      // find local t
      float lt=(t-m_env[i-1].first)/(m_env[i].first-m_env[i-1].first);
      float v=(1-lt)*m_env[i-1].second+lt*m_env[i].second;
      return v;
    }
  }
  // loop!
  //t=0;
  // return the last level  
  return 0; //m_env[m_env.size()-1].second;  
}
